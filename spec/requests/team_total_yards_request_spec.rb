# frozen_string_literal: true

require 'rails_helper'

RSpec.describe TeamTotalYardsSerializer, type: :request do
  describe '#index' do
    context 'teams with players exist' do
      it 'returns serialized json' do
        team_1 = create(:team)
        team_2 = create(:team)
        create(:player, total_rushing_yards: 100, team: team_1, rushing_yards_per_game: 100)
        create(:player, total_rushing_yards: 100, team: team_1, rushing_yards_per_game: 150)
        create(:player, total_rushing_yards: 250, team: team_2, rushing_yards_per_game: 200)
        create(:player, total_rushing_yards: 250, team: team_2, rushing_yards_per_game: 300)

        get teams_total_yards_path(format: :json)

        expect(response).to have_http_status(:ok)
        parsed_body = JSON.parse(response.body)
        expect(parsed_body['data'].first['attributes']['full_name']).to eq(team_2.full_name)
        expect(parsed_body['data'].first['attributes']['total_yards']).to eq(500)
        expect(parsed_body['data'].first['attributes']['average_rushing_yards_per_game']).to eq(250.0)
        expect(parsed_body['data'].second['attributes']['full_name']).to eq(team_1.full_name)
        expect(parsed_body['data'].second['attributes']['total_yards']).to eq(200)
        expect(parsed_body['data'].second['attributes']['average_rushing_yards_per_game']).to eq(125.0)
      end
    end
  end
end
