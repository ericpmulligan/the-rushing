# frozen_string_literal: true

require 'rails_helper'

RSpec.describe PlayersController, type: :request do
  include PlayersHelper
  include ERB::Util

  describe '#index' do
    context 'html request' do
      it 'loads the page' do
        get players_path

        expect(response).to have_http_status(:ok)
        expect(response.body).to include('index.PlayersPage')
      end
    end

    context 'json request' do
      it 'returns a serialized JSON string of players' do
        player_1, player_2 = create_list(:player, 2)

        get players_path(format: :json)

        expect(response).to have_http_status(:ok)
        parsed_body = JSON.parse(response.body)
        expect(parsed_body['data'].first['id']).to eq(player_1.id.to_s)
        expect(parsed_body['data'].second['id']).to eq(player_2.id.to_s)
      end

      it 'returns a serialized JSON string of players sorted by total rushing yards' do
        player_1 = create(:player, total_rushing_yards: 3)
        player_2 = create(:player, total_rushing_yards: 1)
        player_3 = create(:player, total_rushing_yards: 2)

        get players_path(sort_column: 'yds', sort_direction: 'asc', format: :json)

        expect(response).to have_http_status(:ok)
        parsed_body = JSON.parse(response.body)
        expect(parsed_body['data'].first['id']).to eq(player_2.id.to_s)
        expect(parsed_body['data'].second['id']).to eq(player_3.id.to_s)
        expect(parsed_body['data'].third['id']).to eq(player_1.id.to_s)
      end

      it 'returns a serialized JSON string of players sorted by total rushing yards in descending order' do
        player_1 = create(:player, total_rushing_yards: 3)
        player_2 = create(:player, total_rushing_yards: 1)
        player_3 = create(:player, total_rushing_yards: 2)

        get players_path(sort_column: 'yds', sort_direction: 'desc', format: :json)

        expect(response).to have_http_status(:ok)
        parsed_body = JSON.parse(response.body)
        expect(parsed_body['data'].first['id']).to eq(player_1.id.to_s)
        expect(parsed_body['data'].second['id']).to eq(player_3.id.to_s)
        expect(parsed_body['data'].third['id']).to eq(player_2.id.to_s)
      end

      it 'returns a serialized JSON string of players sorted by total rushing touchdowns' do
        player_1 = create(:player, total_rushing_touchdowns: 3)
        player_2 = create(:player, total_rushing_touchdowns: 1)
        player_3 = create(:player, total_rushing_touchdowns: 2)

        get players_path(sort_column: 'td', sort_direction: 'asc', format: :json)

        expect(response).to have_http_status(:ok)
        parsed_body = JSON.parse(response.body)
        expect(parsed_body['data'].first['id']).to eq(player_2.id.to_s)
        expect(parsed_body['data'].second['id']).to eq(player_3.id.to_s)
        expect(parsed_body['data'].third['id']).to eq(player_1.id.to_s)
      end

      it 'returns a serialized JSON string of players sorted by total rushing touchdowns in descending order' do
        player_1 = create(:player, total_rushing_touchdowns: 3)
        player_2 = create(:player, total_rushing_touchdowns: 1)
        player_3 = create(:player, total_rushing_touchdowns: 2)

        get players_path(sort_column: 'td', sort_direction: 'desc', format: :json)

        expect(response).to have_http_status(:ok)
        parsed_body = JSON.parse(response.body)
        expect(parsed_body['data'].first['id']).to eq(player_1.id.to_s)
        expect(parsed_body['data'].second['id']).to eq(player_3.id.to_s)
        expect(parsed_body['data'].third['id']).to eq(player_2.id.to_s)
      end

      it 'returns a serialized JSON string of players sorted by longest rush' do
        player_1 = create(:player, longest_rush: 3)
        player_2 = create(:player, longest_rush: 1)
        player_3 = create(:player, longest_rush: 2)

        get players_path(sort_column: 'lng', sort_direction: 'asc', format: :json)

        expect(response).to have_http_status(:ok)
        parsed_body = JSON.parse(response.body)
        expect(parsed_body['data'].first['id']).to eq(player_2.id.to_s)
        expect(parsed_body['data'].second['id']).to eq(player_3.id.to_s)
        expect(parsed_body['data'].third['id']).to eq(player_1.id.to_s)
      end

      it 'returns a serialized JSON string of players sorted by longest rush in descending order' do
        player_1 = create(:player, longest_rush: 3)
        player_2 = create(:player, longest_rush: 1)
        player_3 = create(:player, longest_rush: 2)

        get players_path(sort_column: 'lng', sort_direction: 'desc', format: :json)

        expect(response).to have_http_status(:ok)
        parsed_body = JSON.parse(response.body)
        expect(parsed_body['data'].first['id']).to eq(player_1.id.to_s)
        expect(parsed_body['data'].second['id']).to eq(player_3.id.to_s)
        expect(parsed_body['data'].third['id']).to eq(player_2.id.to_s)
      end
    end

    context 'csv request' do
      it 'returns a csv representation of the data' do
        players = create_list(:player, 2)

        get players_path(format: :csv)

        expect(response).to have_http_status(:ok)
        expect(response.body.strip).to eq(h(generate_csv(players).strip))
      end
    end
  end
end
