# frozen_string_literal: true

FactoryBot.define do
  factory(:team) do
    short_name { Faker::Sports::Football.team }
    full_name { short_name }
    name { short_name }
    division { Faker::Sports::Football.competition }
    conference { division }
    sequence(:external_id) { |n| n }
    abbreviation { short_name }
  end
end
