# frozen_string_literal: true

FactoryBot.define do
  factory(:player) do
    association(:team)
    name { Faker::Sports::Football.player }
    position { Faker::Sports::Football.position }
  end
end
