# frozen_string_literal: true

require 'rails_helper'

describe Database::Players::Create do
  let(:contents) do
    [
      {
        "Player" => "Joe Banyard",
        "Team" => "JAX",
        "Pos" => "RB",
        "Att" => 2,
        "Att/G" => 2,
        "Yds" => 7,
        "Avg" => 3.5,
        "Yds/G" => 7,
        "TD" => 0,
        "Lng" => "7",
        "1st" => 0,
        "1st%" => 0,
        "20+" => 0,
        "40+" => 0,
        "FUM" => 0
      },
      {
        "Player" => "Shaun Hill",
        "Team" => "MIN",
        "Pos" => "QB",
        "Att" => 5,
        "Att/G" => 1.7,
        "Yds" => 5,
        "Avg" => 1,
        "Yds/G" => 1.7,
        "TD" => 0,
        "Lng" => "9",
        "1st" => 0,
        "1st%" => 0,
        "20+" => 0,
        "40+" => 0,
        "FUM" => 0
      }
    ]
  end

  context 'team does not exist' do
    it 'returns an error and rolls back the transaction' do
      create(:team, abbreviation: 'JAX')

      expect {
        result = described_class.new(contents: contents).call

        expect(result[:status]).to eq(:error)
        expect(result[:error_message]).to eq("Cannot find team MIN")
      }.not_to change { Player.count }
    end
  end

  context 'player already exists' do
    it 'returns an error and rolls back the transaction' do
      create(:team, abbreviation: 'JAX')
      team = create(:team, abbreviation: 'MIN')
      create(:player, team: team, name: "Shaun Hill", position: 'QB')

      expect {
        result = described_class.new(contents: contents).call

        expect(result[:status]).to eq(:error)
        expect(result[:error_message]).to eq("Player Shaun Hill of the #{team.full_name} already exists in the database")
      }.not_to change { Player.count }
    end
  end

  context 'required attribute is missing' do
    it 'returns an error and rolls back the transaction' do
      contents.second['Player'] = nil
      create(:team, abbreviation: 'JAX')
      team = create(:team, abbreviation: 'MIN')

      expect {
        result = described_class.new(contents: contents).call

        expect(result[:status]).to eq(:error)
        expect(result[:error_message]).to eq("Validation failed while creating player  of the #{team.full_name}: Name can't be blank")
      }.not_to change { Player.count }
    end
  end

  context 'valid attributes' do
    it 'successfully creates the players' do
      create(:team, abbreviation: 'JAX')
      create(:team, abbreviation: 'MIN')

      expect {
        result = described_class.new(contents: contents).call

        expect(result[:status]).to eq(:success)
      }.to change { Player.count }.by(2)
    end
  end
end
