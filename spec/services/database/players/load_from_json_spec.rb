# frozen_string_literal: true

require 'rails_helper'

describe Database::Players::LoadFromJson do
  context 'contents are invalid' do
    it 'returns an error' do
      allow(JSON).to receive(:parse).and_return({})

      result = described_class.new.call

      expect(result[:status]).to eq(:error)
      expect(result[:error_message]).to eq("Contents are invalid")
    end
  end

  context 'contents are valid' do
    it 'calls the create service' do
      allow(Database::Players::Create).to receive_message_chain(:new, :call).and_return(status: :success)

      result = described_class.new.call

      expect(result[:status]).to eq(:success)
    end
  end
end
