# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Database::Teams::LoadFromApi do
  context 'successful response', :vcr do
    it 'creates all of the teams' do
      instance = described_class.new

      expect {
        result = instance.call

        expect(result[:status]).to eq(:success)
      }.to change { Team.count }.by(32)
    end
  end

  context 'unsuccessful response' do
    it 'returns an error status' do
      response = instance_spy(HTTParty::Response)
      allow(response).to receive(:code).and_return(404)
      allow(HTTParty).to receive(:get).and_return(response)
      instance = described_class.new

      expect {
        result = instance.call

        expect(result[:status]).to eq(:error)
        expect(result[:error_message]).to eq("Connection to theScore's API failed with status code of 404")
      }.not_to change { Team.count }
    end
  end
end
