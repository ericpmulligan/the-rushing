# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Database::Teams::Create do
  let(:params) do
    {
      teams: [
        {
          "medium_name" => "BUF Bills",
          "short_name" => "BUF",
          "division" => "AFC East",
          "full_name" => "Buffalo Bills",
          "name" => "Bills",
          "conference" => "AFC",
          "id" => 1,
          "abbreviation" => "BUF",
          "logos" => {
            "large" => "https://d1si3tbndbzwz9.cloudfront.net/football/team/1/logo.png",
            "small" => "https://d1si3tbndbzwz9.cloudfront.net/football/team/1/small_logo.png",
            "w72xh72" => "https://d1si3tbndbzwz9.cloudfront.net/football/team/1/w72xh72_logo.png",
            "tiny" => "https://d1si3tbndbzwz9.cloudfront.net/football/team/1/tiny_logo.png"
          }
        },
        {
          "medium_name" => "MIA Dolphins",
          "short_name" => "MIA",
          "division" => "AFC East",
          "full_name" => "Miami Dolphins",
          "name" => "Dolphins",
          "conference" => "AFC",
          "abbreviation" => "MIA",
          "id" => 2,
          "logos" => {
            "large" => "https://d1si3tbndbzwz9.cloudfront.net/football/team/2/logo.png",
            "small" => "https://d1si3tbndbzwz9.cloudfront.net/football/team/2/small_logo.png",
            "w72xh72" => "https://d1si3tbndbzwz9.cloudfront.net/football/team/2/w72xh72_logo.png",
            "tiny" => "https://d1si3tbndbzwz9.cloudfront.net/football/team/2/tiny_logo.png"
          }
        },
        {
          "medium_name" => "NE Patriots",
          "short_name" => "NE",
          "division" => "AFC East",
          "full_name" => "New England Patriots",
          "name" => "Patriots",
          "conference" => "AFC",
          "id" => 3,
          "abbreviation" => "NE",
          "logos" => {
            "large" => "https://d1si3tbndbzwz9.cloudfront.net/football/team/3/logo.png",
            "small" => "https://d1si3tbndbzwz9.cloudfront.net/football/team/3/small_logo.png",
            "w72xh72" => "https://d1si3tbndbzwz9.cloudfront.net/football/team/3/w72xh72_logo.png",
            "tiny" => "https://d1si3tbndbzwz9.cloudfront.net/football/team/3/tiny_logo.png"
          },
        }
      ]
    }
  end

  context 'teams key is missing' do
    it 'returns an error' do
      instance = described_class.new(params: {})

      result = instance.call

      expect(result[:status]).to eq(:error)
      expect(result[:error_message]).to eq("No teams key exists in the passed in parameters")
    end
  end

  context 'A duplicate team already exists' do
    it 'returns an error' do
      Team.create(
        external_id: 1,
        short_name: "BUF",
        full_name: "Buffalo Bills",
        name: "Bills",
        division: "AFC East",
        conference: "AFC",
        abbreviation: "BUF"
      )
      instance = described_class.new(params: params)

      expect {
        result = instance.call

        expect(result[:status]).to eq(:error)
        expect(result[:error_message]).to eq("There seems to be a duplicate BUF")
      }.not_to change { Team.count }
    end
  end

  context 'a team required parameter is missing' do
    it 'returns an error' do
      params[:teams].first['name'] = nil
      instance = described_class.new(params: params)

      expect {
        result = instance.call

        expect(result[:status]).to eq(:error)
        expect(result[:error_message]).to eq("Validation errors occurred while creating team with external id 1: Name can't be blank")
      }.not_to change { Team.count }
    end

    it 'rolls back the transaction and all teams that were just created are removed' do
      params[:teams].second['name'] = nil
      instance = described_class.new(params: params)

      expect {
        result = instance.call

        expect(result[:status]).to eq(:error)
        expect(result[:error_message]).to eq("Validation errors occurred while creating team with external id 2: Name can't be blank")
      }.not_to change { Team.count }
    end
  end

  context 'all teams have all required parameters' do
    it 'creates the records and returns success' do
      instance = described_class.new(params: params)

      expect {
        result = instance.call

        expect(result[:status]).to eq(:success)
      }.to change { Team.count }.by(3)
    end
  end
end
