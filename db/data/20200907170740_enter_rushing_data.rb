# frozen_string_literal: true

class EnterRushingData < ActiveRecord::Migration[6.0]
  def up
    result = Database::Players::LoadFromJson.new.call
    raise "Migration failed with reason: #{result[:error_message]}" if result[:status] == :error
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
