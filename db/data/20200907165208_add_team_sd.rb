# frozen_string_literal: true

class AddTeamSd < ActiveRecord::Migration[6.0]
  def up
    Team.find_or_create_by!(
      short_name: "SD",
      full_name: "San Diego Chargers",
      name: "Chargers",
      division: "AFC West",
      conference: "AFC",
      external_id: 0,
      abbreviation: "SD"
    )
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
