# frozen_string_literal: true

require 'net/http'

class LoadTeams < ActiveRecord::Migration[6.0]
  def up
    result = Database::Teams::LoadFromApi.new.call
    raise "Migration failed with reason: #{result[:error_message]}" if result[:status] == :error
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
