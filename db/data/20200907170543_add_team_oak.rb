# frozen_string_literal: true

class AddTeamOak < ActiveRecord::Migration[6.0]
  def up
    Team.find_or_create_by!(
      short_name: "OAK",
      full_name: "Oakland Raiders",
      name: "Raiders",
      division: "AFC West",
      conference: "AFC",
      external_id: 0,
      abbreviation: "OAK"
    )
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
