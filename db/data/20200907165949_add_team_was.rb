# frozen_string_literal: true

class AddTeamWas < ActiveRecord::Migration[6.0]
  def up
    Team.find_or_create_by!(
      short_name: "WAS",
      full_name: "Washington Redskins",
      name: "Redskins",
      division: "NFC East",
      conference: "NFC",
      external_id: 0,
      abbreviation: "WAS"
    )
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
