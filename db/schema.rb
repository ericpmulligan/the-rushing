# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_09_07_223710) do

  create_table "data_migrations", primary_key: "version", id: :string, force: :cascade do |t|
  end

  create_table "players", force: :cascade do |t|
    t.integer "team_id", null: false
    t.string "name", null: false
    t.string "position", null: false
    t.decimal "rushing_attempts_per_game_average", precision: 5, scale: 1
    t.integer "rushing_attempts"
    t.integer "total_rushing_yards"
    t.decimal "rushing_average_yards_per_attempt", precision: 5, scale: 1
    t.decimal "rushing_yards_per_game", precision: 5, scale: 1
    t.integer "total_rushing_touchdowns"
    t.integer "longest_rush"
    t.integer "rushing_first_downs"
    t.decimal "rushing_first_down_percentage", precision: 5, scale: 1
    t.integer "rushing_20_plus_yards_each"
    t.integer "rushing_40_plus_yards_each"
    t.integer "rushing_fumbles"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.boolean "longest_rush_touchdown", default: false
    t.index ["longest_rush"], name: "index_players_on_longest_rush"
    t.index ["name"], name: "index_players_on_name"
    t.index ["position"], name: "index_players_on_position"
    t.index ["team_id"], name: "index_players_on_team_id"
    t.index ["total_rushing_touchdowns"], name: "index_players_on_total_rushing_touchdowns"
    t.index ["total_rushing_yards"], name: "index_players_on_total_rushing_yards"
  end

  create_table "team_logos", force: :cascade do |t|
    t.integer "team_id", null: false
    t.string "variant", null: false
    t.string "url", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["team_id"], name: "index_team_logos_on_team_id"
    t.index ["variant"], name: "index_team_logos_on_variant"
  end

  create_table "teams", force: :cascade do |t|
    t.string "short_name", null: false
    t.string "full_name", null: false
    t.string "name", null: false
    t.string "division", null: false
    t.string "conference", null: false
    t.integer "external_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "abbreviation", null: false
    t.index ["abbreviation"], name: "index_teams_on_abbreviation"
  end

  add_foreign_key "players", "teams"
  add_foreign_key "team_logos", "teams"
end
