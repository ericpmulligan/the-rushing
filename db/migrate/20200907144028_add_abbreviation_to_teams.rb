# frozen_string_literal: true

class AddAbbreviationToTeams < ActiveRecord::Migration[6.0]
  def change
    add_column :teams, :abbreviation, :string

    add_index :teams, :abbreviation
    remove_index :teams, :short_name
  end
end
