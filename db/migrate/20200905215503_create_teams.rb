# frozen_string_literal: true

class CreateTeams < ActiveRecord::Migration[6.0]
  def change
    create_table :teams do |t|
      t.string :short_name, null: false, index: true
      t.string :full_name, null: false
      t.string :name, null: false
      t.string :division, null: false
      t.string :conference, null: false

      t.bigint :external_id, null: false

      t.timestamps null: false
    end
  end
end
