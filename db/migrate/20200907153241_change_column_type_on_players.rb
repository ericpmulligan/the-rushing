# frozen_string_literal: true

class ChangeColumnTypeOnPlayers < ActiveRecord::Migration[6.0]
  def up
    change_column :players, :rushing_attempts_per_game_average, :decimal, precision: 5, scale: 1
    change_column :players, :rushing_attempts, :integer
    change_column :players, :total_rushing_yards, :integer
    change_column :players, :rushing_average_yards_per_attempt, :decimal, precision: 5, scale: 1
    change_column :players, :rushing_yards_per_game, :decimal, precision: 5, scale: 1
    change_column :players, :total_rushing_touchdowns, :integer
    change_column :players, :longest_rush, :integer
    change_column :players, :rushing_first_downs, :integer
    change_column :players, :rushing_first_down_percentage, :decimal, precision: 5, scale: 1
    change_column :players, :rushing_20_plus_yards_each, :integer
    change_column :players, :rushing_40_plus_yards_each, :integer
    change_column :players, :rushing_fumbles, :integer
  end

  def down
    change_column :players, :rushing_attempts_per_game_average, :string
    change_column :players, :rushing_attempts, :string
    change_column :players, :total_rushing_yards, :string
    change_column :players, :rushing_average_yards_per_attempt, :string
    change_column :players, :rushing_yards_per_game, :string
    change_column :players, :total_rushing_touchdowns, :string
    change_column :players, :longest_rush, :string
    change_column :players, :rushing_first_downs, :string
    change_column :players, :rushing_first_down_percentage, :string
    change_column :players, :rushing_20_plus_yards_each, :string
    change_column :players, :rushing_40_plus_yards_each, :string
    change_column :players, :rushing_fumbles, :string
  end
end
