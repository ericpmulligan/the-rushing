# frozen_string_literal: true

class ChangeColumnNullForAbbreviationOnTeams < ActiveRecord::Migration[6.0]
  def change
    change_column_null :teams, :abbreviation, false
  end
end
