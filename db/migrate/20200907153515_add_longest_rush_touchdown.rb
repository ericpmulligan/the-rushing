# frozen_string_literal: true

class AddLongestRushTouchdown < ActiveRecord::Migration[6.0]
  def change
    add_column :players, :longest_rush_touchdown, :boolean, default: false
  end
end
