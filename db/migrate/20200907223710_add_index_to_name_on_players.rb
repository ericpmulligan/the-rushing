# frozen_string_literal: true

class AddIndexToNameOnPlayers < ActiveRecord::Migration[6.0]
  def change
    add_index :players, :name
  end
end
