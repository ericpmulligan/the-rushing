# frozen_string_literal: true

class CreatePlayers < ActiveRecord::Migration[6.0]
  def change
    create_table :players do |t|
      t.references :team, null: false, index: true, foreign_key: true
      t.string :name, null: false
      t.string :position, null: false, index: true
      t.string :rushing_attempts_per_game_average
      t.string :rushing_attempts
      t.string :total_rushing_yards, index: true
      t.string :rushing_average_yards_per_attempt
      t.string :rushing_yards_per_game
      t.string :total_rushing_touchdowns, index: true
      t.string :longest_rush, index: true
      t.string :rushing_first_downs
      t.string :rushing_first_down_percentage
      t.string :rushing_20_plus_yards_each
      t.string :rushing_40_plus_yards_each
      t.string :rushing_fumbles

      t.timestamps null: false
    end
  end
end
