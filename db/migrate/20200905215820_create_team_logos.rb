# frozen_string_literal: true

class CreateTeamLogos < ActiveRecord::Migration[6.0]
  def change
    create_table :team_logos do |t|
      t.references :team, null: false, index: true, foreign_key: true
      t.string :variant, null: false, index: true
      t.string :url, null: false

      t.timestamps null: false
    end
  end
end
