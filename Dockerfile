FROM ruby:2.6
RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
RUN echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list
RUN apt-get update -qq && apt-get install -y build-essential sqlite3 libsqlite3-dev nodejs yarn

ENV APP_HOME /the-rushing/
RUN mkdir $APP_HOME
WORKDIR $APP_HOME

RUN gem install bundler
ADD Gemfile* $APP_HOME
RUN bundle install --deployment --without development test

ADD . $APP_HOME
ENV RAILS_ENV production
ENV NODE_ENV production
RUN bundle exec rails assets:precompile
RUN bundle exec rails db:create
RUN bundle exec rails db:schema:load
RUN bundle exec rails data:migrate

# Add a script to be executed every time the container starts.
COPY entrypoint.sh /usr/bin/
RUN chmod +x /usr/bin/entrypoint.sh
ENTRYPOINT ["entrypoint.sh"]
EXPOSE 3000

# Start the main process.
CMD ["bundle", "exec", "rails", "server", "-b", "0.0.0.0"]
