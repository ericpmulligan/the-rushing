# frozen_string_literal: true

class PlayersController < ApplicationController
  def index
    scope = Player.includes(:team)
    scope = filter_results(scope)
    @players = order_results(scope)

    respond_to do |format|
      format.html
      format.json { render json: PlayerSerializer.new(@players).serializable_hash, status: :ok }
      format.csv do
        headers['Content-Disposition'] = "attachment; filename=\"players.csv\""
        headers['Content-Type'] ||= 'text/csv'
      end
    end
  end

  private

    def filter_results(scope)
      if params[:q].present?
        scope.where(Player.arel_table[:name].matches("%#{Player.sanitize_sql_like(params[:q])}%"))
      else
        scope
      end
    end

    def order_results(scope)
      case params[:sort_column]
      when 'yds'
        scope.order(total_rushing_yards: params[:sort_direction])
      when 'td'
        scope.order(total_rushing_touchdowns: params[:sort_direction])
      when 'lng'
        scope.order(longest_rush: params[:sort_direction])
      else
        scope
      end
    end
end
