# frozen_string_literal: true

class TeamsTotalYardsController < ApplicationController
  def index
    @teams = Team.select("id, full_name", "teams_total_yards.total_yards, average_rushing_yards_per_game").joins("join (select team_id, sum(total_rushing_yards) as total_yards, avg(rushing_yards_per_game) as average_rushing_yards_per_game from players group by team_id order by total_yards desc) teams_total_yards on teams_total_yards.team_id = teams.id").order(total_yards: :desc)
    render json: TeamTotalYardsSerializer.new(@teams).serializable_hash, status: :ok
  end
end
