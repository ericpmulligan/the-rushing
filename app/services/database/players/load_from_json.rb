# frozen_string_literal: true

module Database::Players
  class LoadFromJson
    def call
      raise_contents_are_invalid unless content_has_players?
      Create.new(contents: contents).call
    rescue ServiceError => ex
      { status: :error, error_message: ex.message }
    end

    private

      def content_has_players?
        contents.count > 0
      end

      def contents
        @contents ||= JSON.parse(File.read("#{Rails.root}/db/files/rushing.json"))
      end

      def raise_contents_are_invalid
        raise ServiceError, "Contents are invalid"
      end
  end
end
