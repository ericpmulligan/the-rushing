# frozen_string_literal: true

module Database::Players
  class Create
    def initialize(contents:)
      @contents = contents
    end

    def call
      create_players

      { status: :success }
    rescue ServiceError => ex
      { status: :error, error_message: ex.message }
    end

    private

      attr_reader :contents

      def create_player(params:)
        team = get_team(abbreviation: params['Team'])

        player = initialize_player(team: team, params: params)
        player.rushing_attempts = params['Att'].to_i
        player.rushing_attempts_per_game_average = params['Att/G'].to_d
        player.total_rushing_yards = params['Yds'].is_a?(String) ? params['Yds'].gsub(',', '').to_i : params['Yds'].to_i
        player.rushing_average_yards_per_attempt = params['Avg'].to_d
        player.rushing_yards_per_game = params['Yds/G'].to_d
        player.total_rushing_touchdowns = params['TD'].to_i
        player.longest_rush = params['Lng'].to_i
        player.longest_rush_touchdown = (params['Lng'].is_a?(String) && params['Lng'].last == 'T') ? true : false
        player.rushing_first_downs = params['1st'].to_i
        player.rushing_first_down_percentage = params['1st%'].to_d
        player.rushing_20_plus_yards_each = params['20+'].to_i
        player.rushing_40_plus_yards_each = params['40+'].to_i
        player.rushing_fumbles = params['FUM'].to_i
        player.save!
      rescue ActiveRecord::RecordInvalid => ex
        raise ServiceError, "Validation failed while creating player #{params['Player']} of the #{team.full_name}: #{ex.record.errors.full_messages.join(', ')}"
      end

      def create_players
        ActiveRecord::Base.transaction do
          contents.each do |params|
            create_player(params: params)
          end
        end
      end

      def get_team(abbreviation:)
        teams = Team.where(abbreviation: abbreviation).or(Team.where(short_name: abbreviation))
        raise ServiceError, "Cannot find team #{abbreviation}" if teams.empty?
        teams.first
      end

      def initialize_player(team:, params:)
        Player.find_or_initialize_by(team: team, name: params['Player'], position: params['Pos']).tap do |player|
          raise ServiceError, "Player #{params['Player']} of the #{team.full_name} already exists in the database" unless player.new_record?
        end
      end
  end
end
