# frozen_string_literal: true

module Database::Teams
  class Create
    def initialize(params: {})
      @params = params
    end

    def call
      ActiveRecord::Base.transaction do
        create_teams
      end

      { status: :success }
    rescue ServiceError => ex
      { status: :error, error_message: ex.message }
    end

    private

      attr_reader :params

      def create_logo(team:, variant:, logos_params:)
        return unless logos_params[variant].present?

        new_or_existing_logo = team.logos.find_or_initialize_by(variant: variant)
        return unless new_or_existing_logo.new_record?

        new_or_existing_logo.tap do |logo|
          logo.url = logos_params[variant]
          logo.save!
        end
      end

      def create_logos(team:, logos_params:)
        create_logo(team: team, variant: 'large', logos_params: logos_params)
        create_logo(team: team, variant: 'small', logos_params: logos_params)
        create_logo(team: team, variant: 'tiny', logos_params: logos_params)
        create_logo(team: team, variant: 'w72xh72', logos_params: logos_params)
      end

      def create_team(team_params:)
        new_or_existing_team = Team.find_or_initialize_by(external_id: team_params['id'])
        raise ServiceError, "There seems to be a duplicate #{team_params['abbreviation']}" unless new_or_existing_team.new_record?

        new_or_existing_team.tap do |team|
          team.short_name = team_params['short_name']
          team.full_name = team_params['full_name']
          team.name = team_params['name']
          team.division = team_params['division']
          team.conference = team_params['conference']
          team.abbreviation = team_params['abbreviation']
          team.save!
        end
      rescue ActiveRecord::RecordInvalid => ex
        raise ServiceError, "Validation errors occurred while creating team with external id #{team_params['id']}: #{ex.record.errors.full_messages.join(', ')}"
      end

      def create_teams
        raise ServiceError, "No teams key exists in the passed in parameters" unless params.key?(:teams)

        params[:teams].each do |team_params|
          team = create_team(team_params: team_params)
          create_logos(team: team, logos_params: team_params['logos'])
        end
      end
  end
end
