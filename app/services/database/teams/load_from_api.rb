# frozen_string_literal: true

module Database::Teams
  class LoadFromApi
    def call
      parsed_teams = get_teams_from_api
      Create.new(params: parsed_teams).call
    rescue ServiceError => ex
      { status: :error, error_message: ex.message }
    end

    private

      def get_teams_from_api
        unless response.code == 200
          raise ServiceError, "Connection to theScore's API failed with status code of #{response.code}"
        end

        { teams: JSON.parse(response.body) }
      end

      def response
        @response ||=
          begin
            HTTParty.get('https://api.thescore.com/nfl/teams/')
          end
      end
  end
end
