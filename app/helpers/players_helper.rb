# frozen_string_literal: true

module PlayersHelper
  def generate_csv(scope)
    CSV.generate do |csv|
      csv << %w(Player Team Pos Att Att/G Yds Avg Yds/G TD Lng 1st 1st% 20+ 40+ FUM)
      scope.each do |player|
        csv << [
          player.name, player.team.full_name, player.position, player.rushing_attempts, player.rushing_attempts_per_game_average,
          player.rushing_average_yards_per_attempt, player.total_rushing_yards, player.rushing_yards_per_game, player.total_rushing_touchdowns,
          "#{player.longest_rush}#{player.longest_rush_touchdown ? 'T' : ''}", player.rushing_first_downs,
          player.rushing_first_down_percentage, player.rushing_20_plus_yards_each, player.rushing_40_plus_yards_each,
          player.rushing_fumbles
        ]
      end
    end
  end
end
