# frozen_string_literal: true

class TeamLogo < ApplicationRecord
  belongs_to :team

  validates :variant, :url, presence: true
end