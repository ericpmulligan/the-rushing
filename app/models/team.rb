# frozen_string_literal: true

class Team < ApplicationRecord
  has_many :logos, class_name: 'TeamLogo'
  has_many :players

  validates :short_name, :full_name, :name, :division, :conference, :external_id, presence: true
end