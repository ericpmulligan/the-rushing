# frozen_string_literal: true

class ApplicationMailer < ActionMailer::Base
  default from: 'eric.pierre.mulligan@gmail.com'
  layout 'mailer'
end
