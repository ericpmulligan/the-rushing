# frozen_string_literal: true

class PlayerSerializer < ApplicationSerializer
  attributes(
    :name, :position, :rushing_attempts_per_game_average, :rushing_attempts, :total_rushing_yards,
    :rushing_average_yards_per_attempt, :rushing_yards_per_game, :total_rushing_touchdowns, :longest_rush,
    :rushing_first_downs, :rushing_first_down_percentage, :rushing_20_plus_yards_each, :rushing_40_plus_yards_each,
    :rushing_fumbles, :longest_rush_touchdown
  )

  attribute :team do |object|
    object.team.full_name
  end
end
