# frozen_string_literal: true

class TeamTotalYardsSerializer < ApplicationSerializer
  attributes :full_name, :total_yards, :average_rushing_yards_per_game
end
