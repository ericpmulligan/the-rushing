import { Action } from "redux";
import { ThunkAction } from "redux-thunk";

import { RootState } from "../reducers";
import { PlayerModel } from "../../types";

export const FETCH_PLAYERS = "Players::FETCH_PLAYERS";
const fetchPlayers = (query: string) => ({
  type: FETCH_PLAYERS,
  query,
});

export const FETCH_PLAYERS_FAILED = "Players::FETCH_PLAYERS_FAILED";
const fetchPlayersFailed = error => ({
  type: FETCH_PLAYERS_FAILED,
  error,
});

export const FETCH_PLAYERS_SUCCEEDED = "Players::FETCH_PLAYERS_SUCCEEDED";
const fetchPlayersSucceeded = (
  players: PlayerModel[],
  sortingColumn = null,
  sortingDirection = null
) => ({
  type: FETCH_PLAYERS_SUCCEEDED,
  players,
  sortingColumn,
  sortingDirection,
});

export const UPDATE_CSV_URL = "Players::UPDATE_CSV_URL";
const updateCsvUrl = (url: string) => ({
  type: UPDATE_CSV_URL,
  url,
});

const fetchPlayersUrl = (query, sortingColumn, sortingDirection = "asc", extension = "json") => {
  const params = new URLSearchParams();
  if (query) {
    params.set("q", query);
  }
  if (sortingColumn) {
    params.set("sort_column", sortingColumn);
    params.set("sort_direction", sortingDirection);
  }
  return `/players.${extension}?${params.toString()}`;
};

export const FETCH_ALL = "Players::FETCH_ALL";
export const fetchAll = (
  query = null,
  sortingColumn = null,
  sortingDirection = null
): ThunkAction<void, RootState, unknown, Action<string>> => dispatch => {
  dispatch(fetchPlayers(query));
  return fetch(fetchPlayersUrl(query, sortingColumn, sortingDirection))
    .then(response => response.json())
    .then(json => {
      dispatch(fetchPlayersSucceeded(json.data, sortingColumn, sortingDirection));
      dispatch(updateCsvUrl(fetchPlayersUrl(query, sortingColumn, sortingDirection, "csv")));
    })
    .catch(error => {
      dispatch(fetchPlayersFailed(error));
    });
};
