import { PlayerModel } from "../../types";
import {
  FETCH_PLAYERS,
  FETCH_PLAYERS_FAILED,
  FETCH_PLAYERS_SUCCEEDED,
  UPDATE_CSV_URL,
} from "./actions";

interface PlayersState {
  csvUrl: string;
  errors: string;
  loading: boolean;
  players: PlayerModel[];
  query: string;
  sortingColumn: string;
  sortingDirection: "asc" | "desc";
}

const initialState: PlayersState = {
  csvUrl: null,
  errors: null,
  players: [],
  loading: false,
  query: null,
  sortingColumn: null,
  sortingDirection: null,
};

const playersReducer = (state = initialState, action): PlayersState => {
  switch (action.type) {
    case FETCH_PLAYERS:
      return Object.assign(
        {},
        {
          ...state,
          query: action.query,
          loading: true,
        }
      );
    case FETCH_PLAYERS_FAILED:
      return Object.assign(
        {},
        {
          ...state,
          errors: action.error,
          loading: false,
        }
      );
    case FETCH_PLAYERS_SUCCEEDED:
      return Object.assign(
        {},
        {
          ...state,
          loading: false,
          players: action.players,
          sortingColumn: action.sortingColumn,
          sortingDirection: action.sortingDirection,
        }
      );
    case UPDATE_CSV_URL:
      return Object.assign(
        {},
        {
          ...state,
          csvUrl: action.url,
        }
      );
    default:
      return state;
  }
};

export { playersReducer };
export type { PlayersState };
