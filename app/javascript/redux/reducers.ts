import { combineReducers } from "redux";

import { playersReducer } from "./players/reducer";

const reducers = combineReducers({
  players: playersReducer,
});

export { reducers };
export type RootState = ReturnType<typeof reducers>;
