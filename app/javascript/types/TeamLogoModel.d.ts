interface TeamLogoModel {
  id: number;
  created_at: Date;
  updated_at: Date;

  variant: string;
  url: string;
}

export type { TeamLogoModel };
