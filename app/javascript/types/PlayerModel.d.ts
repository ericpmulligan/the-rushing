interface PlayerAttributes {
  created_at: Date;
  updated_at: Date;

  name: string;
  position: string;
  rushing_attempts_per_game_average: number;
  rushing_attempts: number;
  total_rushing_yards: number;
  rushing_average_yards_per_attempt: number;
  rushing_yards_per_game: number;
  total_rushing_touchdowns: number;
  longest_rush: number;
  rushing_first_downs: number;
  rushing_first_down_percentage: number;
  rushing_20_plus_yards_each: number;
  rushing_40_plus_yards_each: number;
  rushing_fumbles: number;
  longest_rush_touchdown: boolean;
  team: string;
}

interface PlayerModel {
  id: number;
  type: string;

  attributes: PlayerAttributes;
}

export type { PlayerModel };
