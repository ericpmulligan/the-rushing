import { TeamLogoModel } from "./TeamLogoModel";

interface TeamModel {
  id: number;
  created_at: Date;
  updated_at: Date;

  short_name: string;
  full_name: string;
  name: string;
  division: string;
  conference: string;
  external_id: number;
  abbreviation: string;

  logos: TeamLogoModel[];
}

export type { TeamModel };
