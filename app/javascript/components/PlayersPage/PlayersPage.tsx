import React, { FC } from "react";
import { Provider } from "react-redux";

import { Players } from "./Players";
import { store } from "../../redux/store";

const PlayersPage: FC = () => {
  return (
    <Provider store={store}>
      <Players />
    </Provider>
  );
};

export { PlayersPage };
