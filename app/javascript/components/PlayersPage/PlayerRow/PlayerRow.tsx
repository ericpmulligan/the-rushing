import React, { FC } from "react";
import { PlayerModel } from "../../../types";
import { TableCell, TableRow } from "@material-ui/core";

interface Props {
  player: PlayerModel;
}

const PlayerRow: FC<Props> = ({ player }) => (
  <TableRow>
    <TableCell>{player.attributes.name}</TableCell>
    <TableCell>{player.attributes.team}</TableCell>
    <TableCell>{player.attributes.position}</TableCell>
    <TableCell>{player.attributes.rushing_attempts}</TableCell>
    <TableCell>{player.attributes.rushing_attempts_per_game_average}</TableCell>
    <TableCell>{player.attributes.total_rushing_yards}</TableCell>
    <TableCell>{player.attributes.rushing_average_yards_per_attempt}</TableCell>
    <TableCell>{player.attributes.rushing_yards_per_game}</TableCell>
    <TableCell>{player.attributes.total_rushing_touchdowns}</TableCell>
    <TableCell>
      {player.attributes.longest_rush}
      {player.attributes.longest_rush_touchdown ? "T" : ""}
    </TableCell>
    <TableCell>{player.attributes.rushing_first_downs}</TableCell>
    <TableCell>{player.attributes.rushing_first_down_percentage}</TableCell>
    <TableCell>{player.attributes.rushing_20_plus_yards_each}</TableCell>
    <TableCell>{player.attributes.rushing_40_plus_yards_each}</TableCell>
    <TableCell>{player.attributes.rushing_fumbles}</TableCell>
  </TableRow>
);

export { PlayerRow };
