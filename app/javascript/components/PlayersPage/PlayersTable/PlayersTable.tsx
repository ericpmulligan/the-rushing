import React, { FC } from "react";
import {
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  TableSortLabel,
} from "@material-ui/core";
import { PlayerRow } from "../PlayerRow";
import { PlayerModel } from "../../../types";

interface Props {
  loading: boolean;
  onSortChange: (column: string) => () => void;
  players: PlayerModel[];
  sortingColumn: string;
  sortingDirection: "asc" | "desc";
}

const PlayersTable: FC<Props> = ({
  loading,
  onSortChange,
  players,
  sortingColumn,
  sortingDirection,
}) => (
  <Table size="medium" stickyHeader>
    <TableHead>
      <TableRow>
        <TableCell>Name</TableCell>
        <TableCell>Team</TableCell>
        <TableCell>Position</TableCell>
        <TableCell>Att</TableCell>
        <TableCell>Att/G</TableCell>
        <TableCell sortDirection={sortingColumn === "yds" ? sortingDirection : false}>
          <TableSortLabel
            active={sortingColumn === "yds"}
            direction={sortingColumn === "yds" ? sortingDirection : "asc"}
            onClick={onSortChange("yds")}
          >
            Yds
          </TableSortLabel>
        </TableCell>
        <TableCell>Avg</TableCell>
        <TableCell>Yds/G</TableCell>
        <TableCell sortDirection={sortingColumn === "td" ? sortingDirection : false}>
          <TableSortLabel
            active={sortingColumn === "td"}
            direction={sortingColumn === "td" ? sortingDirection : "asc"}
            onClick={onSortChange("td")}
          >
            TD
          </TableSortLabel>
        </TableCell>
        <TableCell sortDirection={sortingColumn === "lng" ? sortingDirection : false}>
          <TableSortLabel
            active={sortingColumn === "lng"}
            direction={sortingColumn === "lng" ? sortingDirection : "asc"}
            onClick={onSortChange("lng")}
          >
            Lng
          </TableSortLabel>
        </TableCell>
        <TableCell>1st</TableCell>
        <TableCell>1st%</TableCell>
        <TableCell>20+</TableCell>
        <TableCell>40+</TableCell>
        <TableCell>FUM</TableCell>
      </TableRow>
    </TableHead>
    <TableBody>
      {players.map(player => (
        <PlayerRow key={player.id} player={player} />
      ))}
    </TableBody>
  </Table>
);

export { PlayersTable };
