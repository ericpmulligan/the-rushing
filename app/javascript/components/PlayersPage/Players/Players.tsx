import React, { FC, useState } from "react";
import { useSelector, useDispatch } from "react-redux";

import { fetchAll } from "../../../redux/players/actions";
import { RootState } from "../../../redux/reducers";
import { PlayersState } from "../../../redux/players/reducer";
import { PlayersTable } from "../PlayersTable";
import { PlayersFilterForm } from "../PlayersFilterForm";

const Players: FC = () => {
  const [initialized, setInitialized] = useState<boolean>(false);
  const playersState = useSelector<RootState>(state => state.players) as PlayersState;
  const { csvUrl, loading, players, query, sortingColumn, sortingDirection } = playersState;
  const dispatch = useDispatch();

  if (!initialized) {
    setInitialized(true);
    dispatch(fetchAll());
  }

  const handleQuerySubmitted = query => {
    dispatch(fetchAll(query === "" ? null : query, sortingColumn, sortingDirection));
  };

  const handleSortClicked = column => () => {
    let direction = "asc";
    if (column === sortingColumn) {
      direction = sortingDirection === "asc" ? "desc" : "asc";
    }
    dispatch(fetchAll(query, column, direction));
  };

  return (
    <div>
      <PlayersFilterForm
        csvUrl={csvUrl}
        loading={loading}
        onQuerySubmit={handleQuerySubmitted}
        query={query}
      />
      <PlayersTable
        loading={loading}
        onSortChange={handleSortClicked}
        players={players}
        sortingColumn={sortingColumn}
        sortingDirection={sortingDirection}
      />
    </div>
  );
};

export { Players };
