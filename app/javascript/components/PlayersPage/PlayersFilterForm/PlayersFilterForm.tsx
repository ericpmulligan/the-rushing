import React, { ChangeEvent, FC, useState } from "react";
import { Button, createStyles, FormControl, TextField, Theme } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";

interface Props {
  csvUrl: string;
  loading: boolean;
  onQuerySubmit: (query) => void;
  query: string;
}

const useStyles = makeStyles(() =>
  createStyles({
    root: {
      display: "flex",
    },
    download: {
      display: "flex",
      flexGrow: 1,
      alignItems: "center",
      justifyContent: "flex-end",
      textAlign: "right",
    },
    downloadButton: {
      height: 0,
    },
    searchQuery: {
      flexDirection: "row",
    },
  })
);

const PlayersFilterForm: FC<Props> = ({
  csvUrl,
  loading,
  query: queryFromParent,
  onQuerySubmit,
}) => {
  const classes = useStyles();
  const [query, setQuery] = useState<string>(queryFromParent === null ? "" : queryFromParent);

  const handleButtonClicked = () => {
    onQuerySubmit(query);
  };

  const handleQueryChanged = (event: ChangeEvent<HTMLInputElement>) => {
    const { value } = event.target;
    setQuery(value);
  };

  return (
    <div className={classes.root}>
      <FormControl className={classes.searchQuery}>
        <TextField value={query} onChange={handleQueryChanged} label="Search Query" />
        <Button
          onClick={handleButtonClicked}
          disabled={loading}
          variant="contained"
          color="primary"
        >
          Submit
        </Button>
      </FormControl>
      <div className={classes.download}>
        <Button
          variant="contained"
          color="primary"
          disabled={loading || csvUrl === null}
          href={csvUrl}
          target="_blank"
        >
          Download as CSV
        </Button>
      </div>
    </div>
  );
};

export { PlayersFilterForm };
