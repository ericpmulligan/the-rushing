# Installation and running this solution

## Author

Eric Mulligan <eric.pierre.mulligan@gmail.com>

## Running the application

Assuming you have [Docker](https://www.docker.com/get-started) already installed on your computer, run the following from the command line:

```
docker run --name the-rushing -p 3000:3000 -d registry.gitlab.com/ericpmulligan/the-rushing
```

From your browser, go to the following address:

```
http://localhost:3000
```

## Development

The following are requirements for this project:
* Ruby 2.6.6
* Rails 6.0.3.2
* Node v12.18.2

## Clone the repo
To clone the repo, enter the following from the command line:

```
git clone https://gitlab.com/ericpmulligan/the-rushing.git
```

## Installing Ruby 2.6.6
### OSX

Assuming you have [Homebrew](https://brew.sh/) already installed:

From the command line: 
```
brew install ruby-build rbenv
```

Follow the instructions to set up your environment with [rbenv](https://github.com/rbenv/rbenv) on their GitHub repo.

To install `2.6.6`, run:

```
rbenv install 2.6.6
```

Change your current directory to the project by doing:

```
cd the-rushing
```

Once in the project, assuming rbenv is installed correctly on your computer, it should automatically switch your Ruby 
version to 2.6.6. You can verify by doing:

```
ruby --version
```

If rbenv is not installed correctly, please go back to their GitHub repo and follow their troubleshooting steps.

## Installing Node 12.18.2

Assuming you have [Homebrew](https://brew.sh/) already installed:

From the command line: 
```
brew install node-build nodenv
```

Follow the instructions to set up your environment with [nodenv](https://github.com/nodenv/nodenv) on their GitHub repo.

To install `12.18.2`, run:

```
nodenv install 12.18.2
```

Change your current directory to the project by doing if you're not alread there:

```
cd the-rushing
```

Once in the project, assuming nodenv is installed correctly on your computer, it should automatically switch your Node 
version to 12.18.2. You can verify by doing:

```
node --version
```

If nodenv is not installed correctly, please go back to their GitHub repo and follow their troubleshooting steps.

## Installing Yarn

Assuming you have [Homebrew](https://brew.sh/) already installed:

From the command line:

```
brew install yarn
```

## Environment set up

Run the following commands from the `the-rushing` directory:

```
gem install bundler
bundle install
yarn

bin/rails db:migrate
bin/rails data:migrate
```

## Start up the server

Run the following commands from two separate terminal windows from `the-rushing` directory:

```
bin/rails server 
```

and:

```
bin/webpack-dev-server
```
