# frozen_string_literal: true

Rails.application.routes.draw do
  root to: 'players#index'

  resources :players, only: [:index]
  resources :teams_total_yards, only: [:index]
end
